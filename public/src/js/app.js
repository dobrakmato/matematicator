var Map = (function () {
    function Map() {
        this.obj = {
            'foo': 'bar'
        };
    }
    Map.prototype.containsKey = function (key) {
        return this.obj.hasOwnProperty(key);
    };
    Map.prototype.get = function (key) {
        if (this.containsKey(key)) {
            return this.obj[key];
        }
        else {
            return null;
        }
    };
    Map.prototype.put = function (key, value) {
        if (key == null) {
            throw new Error('Key is null!');
        }
        this.obj[key] = value;
    };
    Map.prototype.remove = function (key) {
        delete this.obj[key];
    };
    Map.prototype.size = function () {
        return Object.keys(this.obj).length;
    };
    return Map;
})();
// This file consist of all custom erros.
var ComponentNotFoundError = (function () {
    function ComponentNotFoundError(message) {
        this.name = 'ComponentNotFoundError';
        this.message = message;
    }
    return ComponentNotFoundError;
})();
/// <reference path="map.ts" />
/// <reference path="errors.ts" />
var Application = (function () {
    function Application() {
        this.components = new Map();
    }
    Application.prototype.getComponent = function (type) {
        var name = type.getComponentName();
        if (this.components.containsKey(name)) {
            type.setApplication(this);
            return this.components.get(name);
        }
        else {
            throw new ComponentNotFoundError('This application has no component like "' + name + '" !');
        }
    };
    Application.prototype.addComponent = function (component) {
        this.components.put(component.getComponentName(), component);
    };
    return Application;
})();
var Component = (function () {
    function Component() {
        this.app = null;
    }
    Component.prototype.setApplication = function (app) {
        this.app = app;
    };
    Component.prototype.getComponentName = function () {
        throw new Error('Derived class does not override getComponentName() function!');
    };
    return Component;
})();
// Here are some utility functions.
function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function $(selector) {
    if (selector.charAt(0) === '#') {
        return document.getElementById(selector.substring(1));
    }
    else {
        return null;
    }
}
function degtorad(degrees) {
    return degrees * (Math.PI / 180);
}
function clearInput(functionArgument) {
    if (endsWith(functionArgument, 'deg')) {
        return this.degtorad(parseFloat(functionArgument.substring(0, functionArgument.length - 3)));
    }
    else if (endsWith(functionArgument, 'rad')) {
        return parseFloat(functionArgument.substring(0, functionArgument.length - 3));
    }
    else if (isNumeric(functionArgument)) {
        return parseFloat(functionArgument);
    }
    else {
        return functionArgument;
    }
}
function fact(n) {
    if (n === 1) {
        return 1;
    }
    return n * fact(n - 1);
}
function replaceAll(source, what, replace) {
    return source.split(what).join(replace);
}
function float2fraction(n) {
    n = Math.abs(n);
    var tolerance = 1.e-8, h1 = 1, h2 = 0, k1 = 0, k2 = 1, b = 1 / n;
    do {
        var b = 1 / b;
        var a = Math.floor(b);
        var aux = h1;
        h1 = a * h1 + h2;
        h2 = aux;
        aux = k1;
        k1 = a * k1 + k2;
        k2 = aux;
        b = b - a;
    } while (Math.abs(n - h1 / k1) > n * tolerance);
    return [h1, k1];
}
/// <reference path="../../typings/mathjax/mathjax.d.ts" /> 
/// <reference path="references.ts" />
var MathJaxElement = (function () {
    function MathJaxElement(target) {
        MathJaxElement.counter++;
        this.targetElement = target;
        this.bufferElement = document.createElement('div');
        this.bufferElement.setAttribute('id', 'mathjax-buffer' + MathJaxElement.counter);
        this.bufferElement.style.visibility = 'hidden';
        this.bufferElement.style.position = 'absolute';
        document.body.appendChild(this.bufferElement);
    }
    MathJaxElement.prototype.setText = function (text) {
        this.bufferElement.innerHTML = '`' + text + '`';
        this.draw();
    };
    MathJaxElement.prototype.copyBuffer = function () {
        this.targetElement.innerHTML = this.bufferElement.innerHTML;
    };
    MathJaxElement.prototype.draw = function () {
        var _this = this;
        MathJax.Hub.Queue(['Typeset', MathJax.Hub, this.bufferElement, function () {
                MathJax.Hub.Queue(['copyBuffer', _this]);
            }]);
    };
    MathJaxElement.counter = 0;
    return MathJaxElement;
})();
///<reference path="mathjax.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Whisperer = (function (_super) {
    __extends(Whisperer, _super);
    function Whisperer() {
        var _this = this;
        _super.call(this);
        this.knownFunctions = [];
        this.knownConstants = [];
        this.inputField = $('#search-box');
        this.mathJax = $('#math-jax');
        this.outputElement = $('#result-box');
        this.graphicalResult = $('#graphical-solution');
        this.jsBox = $('#js-box');
        this.mathJaxElement = new MathJaxElement(this.mathJax);
        var objs = Object.getOwnPropertyNames(Math);
        for (var i in objs) {
            if (typeof Math[objs[i]] === 'function') {
                this.knownFunctions.push(objs[i].toLowerCase());
            }
            else if (typeof Math[objs[i]] === 'number') {
                this.knownConstants.push(objs[i].toLowerCase());
            }
        }
        console.debug('Known functions: ' + this.knownFunctions);
        console.debug('Known constants: ' + this.knownConstants);
        this.inputField.addEventListener('keyup', function () {
            var input = _this.inputField.value;
            if (_this.lastInput === input) {
                return;
            }
            _this.lastInput = input;
            if (input !== '') {
                _this.processInput(input);
            }
            else {
                _this.outputElement.innerHTML = '<i>The result will appear here!</i>';
                _this.graphicalResult.setAttribute('src', Whisperer.BLANK_IMAGE);
            }
        });
    }
    Whisperer.prototype.processInput = function (newInput) {
        this.mathJaxElement.setText(newInput);
        var fixed = this.fix(newInput);
        var result = this.safeCompute(fixed, newInput);
        if (result === Whisperer.COMPUTE_ERROR) {
            this.jsBox.innerHTML = fixed;
            this.outputElement.innerHTML = '<b style="color:red">error</b>';
        }
        else if (result === Whisperer.CONTAINS_VARIABILES) {
            this.jsBox.innerHTML = fixed;
            this.outputElement.innerHTML = '<b style="color:orange">see graphical solution</b>';
            this.graphicalResult.setAttribute('src', 'http://185.8.237.158/graphic/graph/soustava_souradnic.php?q='
                + encodeURIComponent(newInput) + '&z=10');
        }
        else {
            this.jsBox.innerHTML = fixed;
            this.outputElement.innerHTML = '<b style="color:green">= ' + result + '</b>';
            if (parseFloat(result) !== parseInt(result, 10)) {
                var fraction = float2fraction(result);
                this.outputElement.innerHTML += ' ( ' + fraction[0] + ' / ' + fraction[1] + ')';
            }
            this.graphicalResult.setAttribute('src', Whisperer.BLANK_IMAGE);
        }
    };
    Whisperer.prototype.isToken = function (str, index) {
        if (index < 0) {
            return false;
        }
        if (index > str.length - 1) {
            return false;
        }
        return Whisperer.TOKENS.indexOf(str[index]) !== -1;
    };
    Whisperer.prototype.tokenReplace = function (source, what, replace) {
        var pos = 0;
        var tries = source.length / what.length;
        while (tries > 0) {
            var index = source.indexOf(what, pos);
            if (index === -1) {
                break;
            }
            if (this.isToken(source, index - 1) && this.isToken(source, index + what.length)) {
                var before = source.substr(0, index);
                var after = source.substr(index + what.length);
                source = before + replace + after;
                pos = index + replace.length;
            }
            else {
                pos = index;
            }
            tries--;
        }
        return source;
    };
    Whisperer.prototype.fix = function (value) {
        console.log('Original query: ' + value);
        value = '#' + value + '#';
        value = value.toLowerCase();
        value = replaceAll(value, ' ', '');
        value = replaceAll(value, ',', '.');
        value = value.replace(Whisperer.DEG_TO_RAD, '(degtorad($1))');
        value = value.replace(Whisperer.FACTORIAL, '(fact($1))');
        value = value.replace(Whisperer.COEF_AND_VAR, '($1 * $2)');
        for (var i = 0; i < this.knownFunctions.length; i++) {
            value = this.tokenReplace(value, this.knownFunctions[i], 'Math.' + this.knownFunctions[i]);
        }
        for (var i = 0; i < this.knownConstants.length; i++) {
            value = this.tokenReplace(value, this.knownConstants[i], 'Math.' + this.knownConstants[i].toUpperCase());
        }
        value = value.replace(Whisperer.POW, 'Math.pow($1, $2)');
        value = value.substring(1, value.length - 1);
        console.log('Fixed query: ' + value);
        return value;
    };
    Whisperer.prototype.compute = function (fixed, original) {
        var result;
        try {
            eval('var result = (' + fixed + ');');
        }
        catch (e) {
            if (e.name === 'ReferenceError') {
                console.debug('Unresolved variables found, using image solution.');
                return Whisperer.CONTAINS_VARIABILES;
            }
            console.error('Can\'t compute result. Error: ' + e);
            return Whisperer.COMPUTE_ERROR;
        }
        console.log('Got result: ' + result);
        return result;
    };
    Whisperer.prototype.safeCompute = function (fixed, original) {
        fixed = fixed.replace('{', '')
            .replace('}', '')
            .replace('document', '.')
            .replace(';', '')
            .replace('window', '.');
        return this.compute(fixed, original);
    };
    Whisperer.prototype.getComponentName = function () {
        return 'Whisperer';
    };
    Whisperer.BLANK_IMAGE = 'data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=';
    Whisperer.COEF_AND_VAR = /([0-9]+)([a-zA-Z]+)/g;
    Whisperer.POW = /([a-zA-Z0-9+\-*/().,]+)\^([a-zA-Z0-9+\-*/().,]+)/;
    Whisperer.FACTORIAL = /([0-9]+)!/g;
    Whisperer.DEG_TO_RAD = /([0-9\.,]+)deg/g;
    Whisperer.CONTAINS_VARIABILES = '#COMPUTE_VARS#';
    Whisperer.COMPUTE_ERROR = '#COMPUTE_ERROR#';
    Whisperer.TOKENS = ['+', '-', '/', '*', '(', ')', ' ', '#'];
    return Whisperer;
})(Component);
/// <reference path="application.ts" />
/// <reference path="component.ts" />
/// <reference path="utils.ts" />
/// <reference path="whisperer.ts" />
var Bootstrap = (function () {
    function Bootstrap() {
        this.app = new Application();
        this.app.addComponent(new Whisperer());
    }
    return Bootstrap;
})();
new Bootstrap;

//# sourceMappingURL=app.js.map