///<reference path="mathjax.ts"/>

class Whisperer extends Component {

    private static BLANK_IMAGE:string = 'data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=';

    private static COEF_AND_VAR:RegExp = /([0-9]+)([a-zA-Z]+)/g;
    private static POW:RegExp = /([a-zA-Z0-9+\-*/().,]+)\^([a-zA-Z0-9+\-*/().,]+)/;
    private static FACTORIAL:RegExp = /([0-9]+)!/g;
    private static DEG_TO_RAD:RegExp = /([0-9\.,]+)deg/g;

    // Compute error codes.
    private static CONTAINS_VARIABILES:string = '#COMPUTE_VARS#';
    private static COMPUTE_ERROR:string = '#COMPUTE_ERROR#';

    // Separating tokens.
    private static TOKENS:string[] = ['+', '-', '/', '*', '(', ')', ' ', '#'];

    // Known functions and constants.
    private knownFunctions:string[] = [];
    private knownConstants:string[] = [];

    // Some HTMLELements.
    private inputField;
    private graphicalResult;
    private mathJax;
    private jsBox;
    private outputElement:HTMLElement;

    private mathJaxElement:MathJaxElement;
    lastInput:string;

    constructor() {
        super();

        // Find search box.
        this.inputField = $('#search-box');
        this.mathJax = $('#math-jax');
        this.outputElement = $('#result-box');
        this.graphicalResult = $('#graphical-solution');
        this.jsBox = $('#js-box');

        // Create MathJax element.
        this.mathJaxElement = new MathJaxElement(this.mathJax);

        // Fill array with supported functions and constants.
        var objs = Object.getOwnPropertyNames(Math);
        for (var i in objs) {
            if (typeof Math[objs[i]] === 'function') {
                this.knownFunctions.push(objs[i].toLowerCase());
            } else if (typeof Math[objs[i]] === 'number') {
                this.knownConstants.push(objs[i].toLowerCase());
            }
        }

        console.debug('Known functions: ' + this.knownFunctions);
        console.debug('Known constants: ' + this.knownConstants);

        // Bind keypress.
        this.inputField.addEventListener('keyup', () => {
            // Get new user input.
            var input = this.inputField.value;
            if (this.lastInput === input) {
                // Do not recalculate same thing over and over.
                return;
            }
            this.lastInput = input;

            // If user typed something.
            if (input !== '') {
                this.processInput(input);
            } else {
                // Otherwise display encouraging message.
                this.outputElement.innerHTML = '<i>The result will appear here!</i>';
                this.graphicalResult.setAttribute('src', Whisperer.BLANK_IMAGE);
            }
        });
    }

    /**
     * Processes new user input. This is called only when input changes.
     *
     * @param newInput input of the user in original form
     */
    private processInput(newInput:string):void {

        // Update math jax.
        this.mathJaxElement.setText(newInput);

        // Fix the input.
        var fixed = this.fix(newInput);
        // Compute result.
        var result = this.safeCompute(fixed, newInput);

        // If we got compute error.
        if (result === Whisperer.COMPUTE_ERROR) {
            // This may be equation or function definition, detect that and take
            // necessary steps to solve this problem.

            // Cry...
            this.jsBox.innerHTML = fixed;
            this.outputElement.innerHTML = '<b style="color:red">error</b>';
        } else if (result === Whisperer.CONTAINS_VARIABILES) {
            // This might be equation, so try to use graphical solution.
            this.jsBox.innerHTML = fixed;
            this.outputElement.innerHTML = '<b style="color:orange">see graphical solution</b>';
            this.graphicalResult.setAttribute('src', 'http://185.8.237.158/graphic/graph/soustava_souradnic.php?q='
                + encodeURIComponent(newInput) + '&z=10');
        } else {
            // We got result, display it.
            this.jsBox.innerHTML = fixed;
            this.outputElement.innerHTML = '<b style="color:green">= ' + result + '</b>';
            if (parseFloat(result) !== parseInt(result, 10)) {
                var fraction = float2fraction(result);
                this.outputElement.innerHTML += ' ( ' + fraction[0] + ' / ' + fraction[1] + ')';
            }
            this.graphicalResult.setAttribute('src', Whisperer.BLANK_IMAGE);
        }
    }

    /**
     * Returns whether character at specified index in specified string is a token.
     * @param str string to check
     * @param index index of char
     * @returns {boolean} true  if char is token char, false otherwise
     */
    private isToken(str:string, index:number):boolean {
        if (index < 0) {
            return false;
        }

        if (index > str.length - 1) {
            return false;
        }

        return Whisperer.TOKENS.indexOf(str[index]) !== -1;
    }

    /**
     * Replaces all occurances of what by replace separated by one of tokens in source string.
     *
     * @param source source string
     * @param what what to search for
     * @param replace what to replace the search to
     * @returns {string} string with replaced content
     */
    private tokenReplace(source:string, what:string, replace:string):string {
        var pos = 0;
        var tries = source.length / what.length;
        // Find all indexes of this constant.
        while (tries > 0) {
            // Find next occurance of constant in input string.
            var index:number = source.indexOf(what, pos);
            // If we can't find this constant anymore.
            if (index === -1) {
                // We are done here, lets get working on other constants.
                break;
            }
            // Check if it is separated by tokens, so we can be sure it is not part of any other statement. For
            // example: ceil(5.2) would result in Math.cMath.Eil(5.2), instead of Math.ceil(5.2).
            if (this.isToken(source, index - 1) && this.isToken(source, index + what.length)) {
                // Replace this occurance.
                var before = source.substr(0, index);
                var after = source.substr(index + what.length);

                source = before + replace + after;
                pos = index + replace.length;
            } else {
                // Move forward.
                pos = index;
            }
            tries--;
        }
        return source;
    }

    /**
     * Converts and fixes specified string to evaluable string.
     * For example this converts 2 ^ 2 to Math.pow(2, 2).
     *
     * @param value original string to fix
     * @returns {string} fixed string
     */
    private fix(value:string):string {
        console.log('Original query: ' + value);

        // Add helpful tokens to start and end of query. We will remove them later.
        value = '#' + value + '#';

        // Lowercase.
        value = value.toLowerCase();

        // Get rid of whitespace.
        value = replaceAll(value, ' ', '');

        // , -> .
        value = replaceAll(value, ',', '.');

        // 90deg -> 1.57
        value = value.replace(Whisperer.DEG_TO_RAD, '(degtorad($1))');

        // 5! -> fact(5)
        value = value.replace(Whisperer.FACTORIAL, '(fact($1))');

        // 2x -> 2*x
        value = value.replace(Whisperer.COEF_AND_VAR, '($1 * $2)');

        // Replace all known Math functions.
        for (var i = 0; i < this.knownFunctions.length; i++) {
            value = this.tokenReplace(value, this.knownFunctions[i], 'Math.' + this.knownFunctions[i]);
        }

        // Replace known mathematical constants.
        for (var i = 0; i < this.knownConstants.length; i++) {
            value = this.tokenReplace(value, this.knownConstants[i], 'Math.' + this.knownConstants[i].toUpperCase());
        }

        // 5^2 -> pow(5, 2)
        value = value.replace(Whisperer.POW, 'Math.pow($1, $2)');

        // Get rid of helpful tokens.
        value = value.substring(1, value.length - 1);

        console.log('Fixed query: ' + value);
        return value;
    }

    // Evaluates expression in fixed string. You should consider using safeCompute() as this method does
    // not checks for unwanted and potentially danger code.
    public compute(fixed:string, original:string):any {
        // This should be refactored to better option.
        // Evaluate this expression.
        var result;
        try {
            eval('var result = (' + fixed + ');');
        } catch (e) {
            // We get a ReferenceError when we evaluate expression with unknown variables.
            if (e.name === 'ReferenceError') {
                console.debug('Unresolved variables found, using image solution.');
                return Whisperer.CONTAINS_VARIABILES;
            }

            // Evaluation just simply failed. Cry now, please.
            console.error('Can\'t compute result. Error: ' + e);
            return Whisperer.COMPUTE_ERROR;
        }
        console.log('Got result: ' + result);
        return result;
    }

    /**
     * Checks input for potentially unsafe code.
     *
     * @param fixed fixed string
     * @param original original string
     * @returns {any} result of evaluation
     */
    public safeCompute(fixed:string, original:string):any {
        fixed = fixed.replace('{', '')
            .replace('}', '')
            .replace('document', '.')
            .replace(';', '')
            .replace('window', '.');

        return this.compute(fixed, original);
    }

    public getComponentName():string {
        return 'Whisperer';
    }
}