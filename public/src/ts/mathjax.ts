/// <reference path="references.ts" />

class MathJaxElement {
    private static counter:number = 0;

    private bufferElement:HTMLElement;
    private targetElement:HTMLElement;

    constructor(target:HTMLElement) {
        MathJaxElement.counter++;

        this.targetElement = target;

        this.bufferElement = document.createElement('div');
        this.bufferElement.setAttribute('id', 'mathjax-buffer' + MathJaxElement.counter);
        this.bufferElement.style.visibility = 'hidden';
        this.bufferElement.style.position = 'absolute';
        document.body.appendChild(this.bufferElement);
    }

    public setText(text:string):void {
        this.bufferElement.innerHTML = '`' + text + '`';
        this.draw();
    }

    public copyBuffer():void {
        this.targetElement.innerHTML = this.bufferElement.innerHTML;
    }

    public draw():void {
        MathJax.Hub.Queue(['Typeset', MathJax.Hub, this.bufferElement, () => {
            MathJax.Hub.Queue(['copyBuffer', this]);
        }]);
    }
}