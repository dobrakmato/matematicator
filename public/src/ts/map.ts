/**
 * Provides simple Map<K, V> implementation using plain javascript object.
 */
class Map<K, V> {
    // Internal map.
    private obj:{};

    /**
     * Constructs a new map.
     */
    constructor() {
        // Create empty object.
        this.obj = {
            'foo': 'bar'
        };
    }

    /**
     * Returns whether this map contains specified key.
     *
     * @param key key of the entry
     * @returns {boolean} true if key exists in this map, false otherwise
     */
    public containsKey(key:K):boolean {
        return this.obj.hasOwnProperty(<any> key);
    }

    /**
     * Returns value for specified key, or null if specified key does not exists in this map. However this will also
     * return null, when value had been previously set to null by using put method.
     *
     * @param key key of the entry
     * @returns {*} value of the entry
     */
    public get(key:K):V {
        if (this.containsKey(key)) {
            return this.obj[<any> key];
        } else {
            return null;
        }
    }

    /**
     * Puts specified value to this map by specified key. Key can't be null, but value can.
     *
     * @param key non-null key of new entry
     * @param value value of new entry
     */
    public put(key:K, value:V) {
        if (key == null) {
            throw new Error('Key is null!');
        }

        this.obj[<any> key] = value;
    }

    /**
     * Removes specified entry from this map by its key.
     *
     * @param key key of entry that should be removed
     */
    public remove(key:K) {
        delete this.obj[<any> key];
    }

    /**
     * Returns size of this Map.
     *
     * @returns {number} number of items in this map
     */
    public size():number {
        return Object.keys(this.obj).length;
    }
}