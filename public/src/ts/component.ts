/**
 * Represents part of Application.
 */
class Component {

    // Instance of application this component belongs to.
    app:Application;

    /**
     * Creates new component instnace.
     */
    constructor() {
        this.app = null;
    }

    /**
     * Sets application field to specified application instance.
     *
     * @param app application instance
     */
    public setApplication(app:Application):void {
        this.app = app;
    }

    /**
     * Returns the name of this component. This method is needed, because our compiler does not
     * keep class names, so we lost control about types at runtime.
     */
    public getComponentName():string {
        throw new Error('Derived class does not override getComponentName() function!');
    }
}