// Here are some utility functions.

/**
 * Returns whether specified string end with specified suffix.
 *
 * @param str input string
 * @param suffix suffix to check
 * @returns {boolean} true if specified input string ends with specified suffix, false otherwise
 */
function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

/**
 * Returns whether specified object is numeric.
 *
 * @param n object to check
 * @returns {boolean} true if object is numeric
 */
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Works like a very simple replacement of jQuery selector method.
 *
 * Currently this only supports element ids.
 *
 * @param selector selector definition
 * @returns {any} html element or null if not found
 */
function $(selector:string):HTMLElement {
    if (selector.charAt(0) === '#') {
        return document.getElementById(selector.substring(1));
    } else {
        return null;
    }
}

/**
 * Converts degreed to radians.
 *
 * @param degrees degrees
 * @returns {number} radians
 */
function degtorad(degrees:number):number {
    return degrees * (Math.PI / 180);
}

// TODO: Use this to clean input to sin/cos functions.
function clearInput(functionArgument:string):number|string {
    if (endsWith(functionArgument, 'deg')) {
        return this.degtorad(parseFloat(functionArgument.substring(0, functionArgument.length - 3)));
    } else if (endsWith(functionArgument, 'rad')) {
        return parseFloat(functionArgument.substring(0, functionArgument.length - 3));
    } else if (isNumeric(functionArgument)) {
        return parseFloat(functionArgument);
    } else {
        return functionArgument;
    }
}

/**
 * Recursively computes factorial.
 *
 * @param n n-factorial
 */
function fact(n:number):number {
    if (n === 1) {
        return 1;
    }
    return n * fact(n - 1);
}

function replaceAll(source:string, what:string, replace:string) {
    // According to http://jsperf.com/replace-all-vs-split-join this method is faster
    // then method with regex with /g.
    return source.split(what).join(replace);
}

/**
 * Converts floating point number to fraction.
 *
 * @param n floating point to get fraction of
 * @returns {any[]} array of two elements of fraction
 */
function float2fraction(n) {
    n = Math.abs(n);
    var tolerance = 1.e-8, h1 = 1, h2 = 0, k1 = 0, k2 = 1, b = 1 / n;
    do {
        var b = 1 / b;
        var a = Math.floor(b);
        var aux = h1;
        h1 = a * h1 + h2;
        h2 = aux;
        aux = k1;
        k1 = a * k1 + k2;
        k2 = aux;
        b = b - a;
    } while (Math.abs(n - h1 / k1) > n * tolerance);
    return [h1, k1];
}