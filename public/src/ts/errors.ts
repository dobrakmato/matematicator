// This file consist of all custom erros.

class ComponentNotFoundError implements Error {
    name:string = 'ComponentNotFoundError';
    message:string;

    constructor(message?:string) {
        this.message = message;
    }
}