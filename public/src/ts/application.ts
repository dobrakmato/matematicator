/// <reference path="map.ts" />
/// <reference path="errors.ts" />

/**
 * Represents main class of client side / browser application.
 */
class Application {
    /**
     * Map of components.
     */
    private components:Map<string, Component>;

    /**
     * Creates a instance of Application main class.
     */
    constructor() {
        this.components = new Map<string, Component>();
    }

    /**
     * Returns component by it's type (prototype).
     *
     * @param type prototype type of component
     * @returns {T} instance of specified component
     */
    public getComponent<T extends Component>(type:T):T {
        var name:string = type.getComponentName();

        if (this.components.containsKey(name)) {
            // Inject application instance.
            type.setApplication(this);
            // Add to components Map.
            return (<T> this.components.get(name));
        } else {
            // Throw error.
            throw new ComponentNotFoundError('This application has no component like "' + name + '" !');
        }
    }

    /**
     * Adds specified component to this application.
     *
     * @param component component to add to application
     */
    public addComponent(component:Component):void {
        this.components.put(component.getComponentName(), component);
    }
}