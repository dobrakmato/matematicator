'use strict';
var GulpConfig = (function () {
    function GulpConfig() {
        // -------- SOURCES ---------

        // Source root.
        this.sourceBase = './src/';
        // Path to javascript soruce files.
        this.sourceJavascript = [this.sourceBase + '/js/**/*.js'];
        this.sourceJavascriptFolder = this.sourceBase + '/js/';
        // Path to typescript source files.
        this.sourceTypescript = [this.sourceBase + '/ts/**/*.ts'];
        // Path to sass source files.
        this.sourceSass = [this.sourceBase + '/sass/**/*.sass',
            this.sourceBase + '/scss/**/*.scss'];
        // Path to source image file.
        this.sourceImages = [this.sourceBase + '/img/**/*.jpg',
            this.sourceBase + '/img/**/*.png',
            this.sourceBase + '/img/**/*.gif',
            this.sourceBase + '/img/**/*.svg']

        // --------- COMPILED ---------
        this.distBase = './dist/';
        // Path to compiled sass.
        this.distCss = this.distBase + '/css';
        this.distJavascript = this.distBase + '/js';
        this.distImages = this.distBase + '/img';

        this.libraryTypeScriptDefinitions = './typings/**/*.ts';
    }

    return GulpConfig;
})();
module.exports = GulpConfig;